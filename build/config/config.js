"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Configurator = void 0;
var instagram_1 = require("../bots/services/instagram/instagram");
var configjson = __importStar(require("../config.json"));
var Configurator = /** @class */ (function () {
    function Configurator() {
        this.configuration = configjson;
    }
    Configurator.prototype.getConfiguration = function () {
        return this.configuration;
    };
    Configurator.getServiceClass = function (service, context) {
        switch (service.service) {
            case "instagram":
                return new instagram_1.Instagram(context, service);
            default:
                throw new Error("Service class not found");
        }
    };
    return Configurator;
}());
exports.Configurator = Configurator;
