"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceBot = void 0;
var accounts_1 = require("../../stores/accounts");
var ServiceBot = /** @class */ (function () {
    function ServiceBot(context, service) {
        this.context = context;
        this.service = service;
        this.db = new accounts_1.AccountsStore();
    }
    ServiceBot.prototype.saveCookies = function (uuid, cookies) {
        this.db.object()
            .push({ uuid: uuid, cookies: cookies })
            .write();
    };
    ServiceBot.prototype.getCookies = function (uuid) {
        return this.db.find({ uuid: uuid })
            .get("cookies")
            .value();
    };
    ServiceBot.prototype.getContext = function () {
        return this.context;
    };
    return ServiceBot;
}());
exports.ServiceBot = ServiceBot;
