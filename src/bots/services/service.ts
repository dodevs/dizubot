import { BrowserContext, Cookie } from 'puppeteer'
import { Service } from '../../models/config.model';

import { AccountsStore } from '../../stores/accounts';

export abstract class ServiceBot {
    protected context: Promise<BrowserContext>;
    protected service: Service;
    protected db: AccountsStore;

    constructor(context: Promise<BrowserContext>, service: Service) {
        this.context = context;
        this.service = service;

        this.db = new AccountsStore();
    }

    abstract launch(): Promise<void>

    protected saveCookies(uuid: string, cookies: Cookie[]) {
        this.db.object()
            .push({uuid, cookies})
            .write();
    }

    protected getCookies(uuid: string): Cookie[] {
        return this.db.find({uuid: uuid})
            .get("cookies")
            .value();
    }

    public getContext(): Promise<BrowserContext>{
        return this.context;
    }
}