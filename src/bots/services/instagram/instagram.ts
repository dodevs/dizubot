import { LowdbAsync } from "lowdb";
import { BrowserContext } from "puppeteer";
import { Account, Service } from "../../../models/config.model";
import { ServiceBot } from "../service";

export class Instagram extends ServiceBot {

    constructor(context: Promise<BrowserContext>, service: Service) {
        super(context, service);
    }

    async launch(): Promise<void> {
       this.login();
    }

    private login() {
        this.context.then(async ctx => {
            const page = await ctx.newPage()
            const instaLogin = await page.goto("https://www.instagram.com/accounts/login/", {timeout: 0, waitUntil: 'load'});
            page.waitForSelector("form#loginForm", {timeout: 10000}).then(async elh => {
                await page.type('input[name="username"]', this.service.account.username);
                await page.type('input[name="password"]', this.service.account.password);
                await page.click('button[type="submit"]');

                page.cookies().then(cookies => this.saveCookies(this.service.uuid, cookies) );
            })
        })
    }
    
}