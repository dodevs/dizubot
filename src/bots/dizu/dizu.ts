import { BrowserContext } from 'puppeteer'
import { ServiceBot } from '../services/service';

export class Dizu {
    private service: ServiceBot
    private context: Promise<BrowserContext>;

    constructor(service: ServiceBot) {
        this.service = service;
        this.context = service.getContext();
    }

    public launch() {

    }
}