import { Configurator } from "./config/config"
import { AccountsStore } from './stores/accounts'
import { launch } from 'puppeteer'

export class Main {
    public main() {
        const configuration = new Configurator().getConfiguration();

        (async () => {
            const browser = await launch();
            configuration.services.forEach(service => {
                const context = browser.createIncognitoBrowserContext();
                const serviceClass = Configurator.getServiceClass(service, context);

                serviceClass.launch();
            });
        })();

    }
}

if (require.main === module) {
    new Main().main();
}