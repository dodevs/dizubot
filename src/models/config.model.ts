export type ServiceName = | "instagram" | "twitter" | "tiktok" | undefined;

export interface Service {
    uuid: string,
    service: ServiceName,
    account: Account
}

export interface Account {
    username: string,
    password: string
}

export interface Config {
    uuid: string,    
    account: Account,
    services: Service[]
}