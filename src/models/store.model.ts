import { Cookie } from "puppeteer";

export interface AccountStore {
    uuid: string,
    cookies: Cookie[]
}

export interface Store {
    accounts: AccountStore[]
}