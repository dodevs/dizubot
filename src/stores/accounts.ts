import { CollectionChain } from 'lodash';
import { AccountStore } from "../models/store.model";
import { Base } from "./base";

export class AccountsStore extends Base {
    constructor() {
        super();
    }

    object() {
        return (super.getObject("accounts") as CollectionChain<AccountStore>);
    }

    find(object: any) {
        return this.object().find(object);
    }
}