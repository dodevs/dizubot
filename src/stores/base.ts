import { LoDashExplicitWrapper, PrimitiveChain } from "lodash";
import Lowdb, * as Low from "lowdb";
import FileSync from "lowdb/adapters/FileSync";
import { Store } from "../models/store.model";

export class Base {

    protected db!: Low.LowdbSync<any>;

    constructor() {
        const adapter = new FileSync("db.json");
        this.db = Lowdb(adapter);

        if (!this.db.has("accounts").value()) {
            this.db.defaults<Store>({
                accounts: []
            }).write();
        }
    }

    public getObject(object: string) {
        if (this.db.has(object).value()) {
            return this.db.get(object);
        }
        else throw new Error("Object not found");
    }
    

}