import { BrowserContext } from "puppeteer";
import { Instagram } from "../bots/services/instagram/instagram";
import { Account, Config, Service } from "../models/config.model";
import * as configjson from '../config.json';

export class Configurator {
    private configuration: Config;

    constructor() {
        this.configuration = configjson as Config;
    }

    public getConfiguration() {
        return this.configuration;
    }

    public static getServiceClass(service: Service, context: Promise<BrowserContext>) {
        switch (service.service) {
            case "instagram":
                return new Instagram(context, service);
        
            default:
                throw new Error("Service class not found");
                
        }
    }
}