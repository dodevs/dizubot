import Lowdb, * as low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';

export class Db {
    private static instance: Db;

    private _db!: low.LowdbSync<any>;

    constructor() {
        const adapter = new FileSync('db.json');
        this._db = Lowdb(adapter);
        
        if (!this._db.has("cookies").value()) {
            this._db.defaults({
                cookies: []
            }).write();
        }
    }

    public getObject(object: string) {
        if (this._db.has(object).value()) {
            return this._db.get(object);
        }
        else throw new Error("Object not found");
    }

    public static getInstance(): Db {
        if (!Db.instance) {
            Db.instance = new Db();
        }

        return Db.instance;
    }
}