# Build application
FROM node:12 AS build

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install
RUN npm run build

# RUn application
FROM build
WORKDIR /usr/src/app/build

CMD [ "node", "index.js"]